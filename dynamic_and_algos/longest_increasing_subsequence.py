from collections import defaultdict


def calc_lis(arr):
    L = defaultdict(list)
    L[0].append(arr[0])

    for i in range(1, len(arr)):
        for j in range(0, i):
            if arr[i] > arr[j] and (len(L[i]) < len(L[j]) + 1):
                L[i] = L[j][:]
        L[i].append(arr[i])

    max_len = 0
    for a_key in L.keys():
        max_len = len(L[a_key]) if len(L[a_key]) > max_len else max_len

    print L

    return max_len


def calc_lis_n(arr):

    L = defaultdict(int)
    L[0] = 1

    max_len = 0
    for i in range(1, len(arr)):
        for j in range(0, i):
            if arr[i] > arr[j] and (L[i] < L[j] + 1):
                L[i] = L[j]
        L[i] = L[i] + 1
        max_len = L[i] if max_len < L[i] else max_len

    return max_len


arr_ = [10, 22, 9, 33, 21, 50, 41, 60]
print calc_lis(arr_)
# print calc_lis_n(arr_)
