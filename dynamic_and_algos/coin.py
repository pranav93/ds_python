def get_combinations(amount, coins):
    combinations = [0] * (amount + 1)
    combinations[0] = 1

    for a_coin in coins:
        for i in range(1, len(combinations)):
            if i >= a_coin:
                combinations[i] += combinations[i - a_coin]

    return combinations[amount]


def get_combinations_rec(amount, coins, current_coin):
    if amount == 0:
        return 1
    if amount < 0:
        return 0

    all_combinations = 0
    for i in range(current_coin, len(coins)):
        all_combinations += get_combinations_rec(amount - coins[i], coins, i)

    return all_combinations


amount_ = 4
coins_ = [1, 2, 3]
print(get_combinations_rec(amount_, coins_, 0))
print(get_combinations(amount_, coins_))
