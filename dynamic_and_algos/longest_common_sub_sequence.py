def lcs(arg1, arg2, len1, len2):
    if len1 == 0 or len2 == 0:
        return 0
    if arg1[len1-1] == arg2[len2-1]:
        return 1 + lcs(arg1, arg2, len1 - 1, len2 - 1)
    else:
        return max(lcs(arg1, arg2, len1 - 1, len2), lcs(arg1, arg2, len1, len2 - 1))


str1 = 'AGGTAB'
str2 = 'GXTXAYB'
len1 = len(str1)
len2 = len(str2)

print lcs(str1, str2, len1, len2)
