def fibo(n, lookup):
    if n == 0 or n == 1:
        lookup[n] = 1

    if lookup.get(n) is None:
        lookup[n] = fibo(n-1, lookup) + fibo(n-2, lookup)

    return lookup[n]


lookup = {}
print fibo(5, lookup)
