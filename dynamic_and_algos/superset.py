set_ = {1, 2, 3, 4, 5, 6, 7}
superset_len = pow(2, len(set_))

and_arr = len(set_) * [0]


def perform_bin_add(and_arr):
    len_and_arr = len(and_arr) - 1
    carry = True
    while len_and_arr >= 0 and carry:
        if carry:
            res = and_arr[len_and_arr] + 1
        if res % 2:
            carry = False
        else:
            carry = True
        and_arr[len_and_arr] = res % 2
        len_and_arr -= 1

    return and_arr


while superset_len > 0:
    and_arr = perform_bin_add(and_arr)
    print {i for i, j in zip(list(set_), and_arr) if j}
    superset_len -= 1
