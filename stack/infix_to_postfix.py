from stack import Stack


class StackInfixToPostfix(Stack):

    def __init__(self, *args, **kwargs):
        super(StackInfixToPostfix, self).__init__(*args, **kwargs)
        self.output = []
        self.priority = {
            '+': 1,
            '-': 1,
            '*': 2,
            '/': 2,
            '^': 3,
        }

    def is_operand(self, char):
        return char.isalpha()

    def has_more_priority(self, char):
        try:
            return self.priority[self.peek()] < self.priority[char]
        except KeyError:
            return True

    def convert_to_postfix(self, exp):
        for i in exp:
            if self.is_operand(i):
                self.output.append(i)
            elif i == '(':
                self.push(i)
            elif i == ')':
                while (not self.is_empty()) and (self.peek() != '('):
                    self.output.append(self.pop())
                if (not self.is_empty()) and (self.peek() == '('):
                    self.pop()
                else:
                    print 'Wrong exp'
            elif self.is_empty() or self.has_more_priority(i):
                self.push(i)
            elif not self.has_more_priority(i):
                while (not self.is_empty()) and (not self.has_more_priority(i)):
                    self.output.append(self.pop())
                self.push(i)

        while not self.is_empty():
            self.output.append(self.pop())

        return self.output


exp = "a+b*(c^d-e)^(f+g*h)-i"
s = StackInfixToPostfix()
print ''.join(s.convert_to_postfix(exp))
