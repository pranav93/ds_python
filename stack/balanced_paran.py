from stack import Stack


class StackBalancedParan(Stack):

    def __init__(self, *args, **kwargs):
        super(StackBalancedParan, self).__init__(self, *args, **kwargs)
        self.output = False
        self.map = {
            ")": "(",
            "}": "{",
            "]": "[",
        }

    def balanced(self, expr):
        for i in expr:
            if i in ['(', '{', '[']:
                self.push(i)
            elif i in [')', '}', ']']:
                if self.map[i] == self.peek():
                    self.pop()
                    self.output = True
                else:
                    self.output = False
                    break
        if self.is_empty():
            self.output = True

        return self.output


print StackBalancedParan().balanced("[()]{}{[()()]()}")
