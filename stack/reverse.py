from stack import Stack


class StackReverse(Stack):
    def __init__(self, *args, **kwargs):
        super(StackReverse, self).__init__(*args, **kwargs)
        self.output = ''

    def reverse(self, expr):
        for i in expr:
            self.push(i)
        while not self.is_empty():
            self.output += self.pop()
        return self.output


s = StackReverse()
print s.reverse('hello')
