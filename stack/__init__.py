class Stack(object):

    def __init__(self, *args, **kwargs):
        self.top = -1
        self.array = []

    def is_empty(self):
        return self.top == -1

    def peek(self):
        return self.array[self.top]

    def push(self, elem):
        self.top += 1
        self.array.append(elem)

    def pop(self):
        if not self.is_empty():
            self.top -= 1
            return self.array.pop()
        else:
            raise ValueError('Stack underflow')
