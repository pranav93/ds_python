class Queue(object):

    def __init__(self):
        self.array = []
        self.tail = -1

    def enq(self, data):
        self.array.append(data)
        self.tail += 1

    def deq(self):
        if self.is_empty():
            raise ValueError('Queue is empty')
        self.tail -= 1
        return self.array.pop(0)

    def is_empty(self):
        return self.tail == -1


class Stack(object):

    def __init__(self):
        self.queue1 = Queue()
        self.queue2 = Queue()

    def push(self, data):
        self.queue1.enq(data)

    def pop(self):
        while not self.queue1.tail == 0:
            deq_data = self.queue1.deq()
            self.queue2.enq(deq_data)
        pop_data = self.queue1.deq()
        self.queue1, self.queue2 = self.queue2, self.queue1
        return pop_data

    def peek(self):
        deq_data = None
        while not self.queue1.is_empty():
            deq_data = self.queue1.deq()
            self.queue2.enq(deq_data)
        self.queue1, self.queue2 = self.queue2, self.queue1
        return deq_data


s = Stack()
s.push(1)
s.push(2)
s.push(3)
s.push(4)
print(s.peek())
print(s.pop())
print(s.pop())
print(s.pop())
print(s.pop())
print(s.peek())
