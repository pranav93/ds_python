from stack import Stack


class StackEvalPostfix(Stack):

    def __init__(self, *args, **kwargs):
        super(StackEvalPostfix, self).__init__(self, *args, **kwargs)
        self.output = 0

    def evaluate(self, expr):
        for i in expr.split(" "):
            if i.isdigit():
                self.push(i)
            else:
                opr1 = self.pop()
                opr2 = self.pop()
                self.output = eval(opr2 + i + opr1)
                self.push(str(self.output))
        return self.output


s = StackEvalPostfix()
print s.evaluate("2 3 1 * + 9 -")
