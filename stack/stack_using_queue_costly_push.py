class Queue(object):

    def __init__(self):
        self.array = []
        self.tail = -1

    def enq(self, data):
        self.array.append(data)
        self.tail += 1

    def deq(self):
        if self.is_empty():
            raise ValueError('Queue is empty')
        self.tail -= 1
        return self.array.pop(0)

    def peek_rear(self):
        return self.array[0] if self.tail != -1 else None

    def is_empty(self):
        return self.tail == -1


class Stack(object):

    def __init__(self):
        self.queue1 = Queue()
        self.queue2 = Queue()

    def push(self, data):
        while not self.queue1.is_empty():
            self.queue2.enq(self.queue1.deq())
        self.queue1.enq(data)
        while not self.queue2.is_empty():
            self.queue1.enq(self.queue2.deq())

    def pop(self):
        return self.queue1.deq()

    def peek(self):
        return self.queue1.peek_rear()


s = Stack()
s.push(1)
s.push(2)
s.push(3)
s.push(4)
print(s.peek())
print(s.pop())
print(s.pop())
print(s.pop())
print(s.pop())
print(s.peek())
