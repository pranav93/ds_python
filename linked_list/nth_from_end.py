class Node(object):

    def __init__(self, data):
        self.data = data
        self.next_ = None


class LinkedList(object):

    def __init__(self):
        self.head = None
        self.calc_ = 0

    def push(self, node):
        node.next_ = self.head
        self.head = node

    def traverse(self):
        current_node = self.head
        while current_node:
            print(current_node.data)
            current_node = current_node.next_

    def __len__(self):
        current_node = self.head
        length = 0
        while current_node:
            length += 1
            current_node = current_node.next_
        return length

    def nth_from_end(self, length, n):
        current_node = self.head
        nth_from_end_index = length - n
        while current_node and nth_from_end_index:
            current_node = current_node.next_
            nth_from_end_index -= 1
        if not current_node:
            return None
        return current_node

    def nth_from_end_ref(self, n):
        current_node = self.head
        ref_node = self.head
        while ref_node and n:
            ref_node = ref_node.next_
            n -= 1
        if not ref_node:
            return None
        while ref_node:
            ref_node = ref_node.next_
            current_node = current_node.next_
        return current_node

    def nth_recursive(self, node, calc_length):
        if self.calc_ == calc_length:
            return node
        if not node:
            return None
        self.calc_ += 1
        return self.nth_recursive(node.next_, calc_length)


l = LinkedList()
node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
l.push(node3)
l.push(node2)
l.push(node1)
l.push(node0)

# length_l = len(l)
# obj = l.nth_from_end(length_l, 5)
# data = obj.data if obj else None
# print(data)

# obj = l.nth_from_end_ref(0)
# data = obj.data if obj else None
# print(data)

calc_length_ = len(l) - 2
obj = l.nth_recursive(l.head, calc_length_)
data = obj.data if obj else None
print(data)
