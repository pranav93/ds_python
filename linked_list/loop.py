class Node(object):

    def __init__(self, data):
        self.data = data
        self.next_ = None
        self.visited = False


class LinkedList(object):

    def __init__(self):
        self.head = None

    def push(self, node):
        node.next_ = self.head
        self.head = node

    def check_loop(self):
        current_node = self.head
        while current_node:
            if not current_node.visited:
                current_node.visited = True
                current_node = current_node.next_
            else:
                return True
        return False


node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)
node5 = Node(5)
ll = LinkedList()

ll.push(node5)
ll.push(node4)
ll.push(node3)
ll.push(node2)
ll.push(node1)
ll.push(node0)
node5.next_ = node2

print ll.check_loop()
