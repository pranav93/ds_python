class Node(object):

    def __init__(self, data):
        self.data = data
        self.next_ = None


class LinkedList(object):

    def __init__(self):
        self.head = None
        self.calc_ = 0

    def push(self, node):
        node.next_ = self.head
        self.head = node

    def traverse(self):
        current_node = self.head
        while current_node:
            print(current_node.data)
            current_node = current_node.next_

    def __len__(self):
        current_node = self.head
        length = 0
        while current_node:
            length += 1
            current_node = current_node.next_
        return length

    def get_nth(self, n):
        current_node = self.head
        while n and current_node:
            current_node = current_node.next_
            n -= 1
        return current_node

    def get_middle(self):
        slow_node = self.head
        fast_node = self.head
        move_slow = True
        while fast_node:
            fast_node = fast_node.next_
            move_slow = not move_slow
            if move_slow:
                slow_node = slow_node.next_
        return slow_node


l = LinkedList()
node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)
node5 = Node(5)

l.push(node5)
l.push(node4)
l.push(node3)
l.push(node2)
l.push(node1)
l.push(node0)

obj = l.get_middle()
data = obj.data if obj else None
print(data)
