class Node(object):

    def __init__(self, data):
        self.data = data
        self.next_ = None


class LinkedList(object):

    def __init__(self):
        self.head = None

    def push(self, node):
        node.next_ = self.head
        self.head = node

    def traverse(self):
        current_node = self.head
        while current_node:
            print(current_node.data)
            current_node = current_node.next_

    def delete_node(self, key):
        current_node = self.head
        prev_node = None
        next_node = current_node.next_
        while current_node:
            if current_node.data == key:
                prev_node.next_ = next_node
                current_node.next_ = None
                return
            prev_node = current_node
            current_node = current_node.next_
            next_node = current_node.next_


l = LinkedList()
node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)
node5 = Node(5)

l.push(node5)
l.push(node4)
l.push(node3)
l.push(node2)
l.push(node1)
l.push(node0)

l.traverse()
print('\n')
l.delete_node(3)
l.delete_node(5)
l.traverse()
print('\n')
