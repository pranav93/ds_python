class Node(object):

    def __init__(self, data):
        self.data = data
        self.next_ = None


class LinkedList(object):

    def __init__(self):
        self.head = None

    def push(self, node):
        node.next_ = self.head
        self.head = node

    def traverse(self):
        current_node = self.head
        while current_node:
            print(current_node.data)
            current_node = current_node.next_

    def insert_after(self, node, data):
        new_node = Node(data)
        new_node.next_ = node.next_
        node.next_ = new_node

    def add_to_end(self, data):
        new_node = Node(data)
        if not self.head:
            self.head = new_node
        else:
            last_node = self.head
            while last_node.next_:
                last_node = last_node.next_
            last_node.next_ = new_node


l = LinkedList()
node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
l.push(node0)
l.push(node1)
l.push(node2)
l.push(node3)

# l.traverse()

l.insert_after(node1, 5)
# l.traverse()

l.add_to_end(18)
l.traverse()
