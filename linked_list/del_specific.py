import gc


class Node(object):

    def __init__(self, data):
        self.data = data
        self.next_ = None


class LinkedList(object):

    def __init__(self):
        self.head = None

    def push(self, node):
        node.next_ = self.head
        self.head = node

    def traverse(self):
        current_node = self.head
        while current_node:
            print(current_node.data)
            current_node = current_node.next_

    def delete_node(self, node):
        temp = node.next_
        node.data = temp.data
        node.next_ = temp.next_
        gc.collect()


l = LinkedList()
node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)

l.push(node0)
l.push(node1)
l.push(node2)
l.push(node3)
l.traverse()
print("\n")

l.delete_node(node2)
l.traverse()
print("\n")
