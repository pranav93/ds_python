class Node(object):

    def __init__(self, data):
        self.data = data
        self.next_ = None


class LinkedList(object):

    def __init__(self):
        self.head = None

    def push(self, node):
        node.next_ = self.head
        self.head = node

    def traverse(self):
        current_node = self.head
        while current_node:
            print(current_node.data)
            current_node = current_node.next_

    def reverse(self):
        prev = None
        current = self.head

        while current:
            next_ = current.next_
            current.next_ = prev
            prev = current
            current = next_

        self.head = prev

    def rec_reverse(self, node):
        if not node:
            return
        first = node
        rest = node.next_
        if not rest:
            self.head = first
            return
        self.rec_reverse(rest)
        first.next_.next_ = first
        first.next_ = None

    def tail_rec_reverse(self, prev, current):
        if not current:
            return
        if not current.next_:
            self.head = current
            current.next_ = prev
            return
        next_ = current.next_
        current.next_ = prev
        self.tail_rec_reverse(current, next_)


node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)
ll = LinkedList()

ll.push(node4)
ll.push(node3)
ll.push(node2)
ll.push(node1)
ll.push(node0)

ll.traverse()
print("\n")
# ll.reverse(ll.head)
# ll.rec_reverse(ll.head)
ll.tail_rec_reverse(None, ll.head)
ll.traverse()
print("\n")
