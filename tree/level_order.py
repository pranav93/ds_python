class Node(object):
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def level_order_traversal(self):
        h = self.height(self)
        if h == 0:
            return None
        [self.print_level(self, i) for i in range(h)]

    def print_level(self, node, level):
        if node:
            if level == 0:
                print node.data
            else:
                self.print_level(node.left, level - 1)
                self.print_level(node.right, level - 1)

    def height(self, passed_node):
        if passed_node is None:
            return 0
        left_height = 1 + self.height(passed_node.left)
        right_height = 1 + self.height(passed_node.right)
        if left_height > right_height:
            return left_height
        else:
            return right_height


root = Node(2)
root.left = Node(1)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(6)
root.right.left = Node(2)
root.right.right = Node(5)

print "Level order traversal of binary tree is -"
root.level_order_traversal()
