class Node(object):

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def traverse_inorder(self, node):
        if node and node.data:
            self.traverse_inorder(node.left)
            print(node.data)
            self.traverse_inorder(node.right)

    def traverse_preorder(self, node):
        if node and node.data:
            print(node.data)
            self.traverse_preorder(node.left)
            self.traverse_preorder(node.right)

    def traverse_postorder(self, node):
        if node and node.data:
            self.traverse_postorder(node.left)
            self.traverse_postorder(node.right)
            print(node.data)


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)

print "Preorder traversal of binary tree is -"
root.traverse_preorder(root)
print "Postorder traversal of binary tree is -"
root.traverse_postorder(root)
print "Inorder traversal of binary tree is -"
root.traverse_inorder(root)
