import sys


class Node(object):

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def is_bst_util(root):
    return is_bst(root, -sys.maxint, sys.maxint)


def is_bst(node, min_, max_):
    if not node:
        return True
    if (node.data > max_) or (node.data < min_):
        return False
    return is_bst(node.left, min_, node.data - 1) and is_bst(node.right, node.data + 1, max_)


def simple_bst(root, left=None, right=None):
    if not root:
        return True
    if left and (root.data < left.data):
        return False
    if right and (root.data > right.data):
        return False
    return simple_bst(root.left, left, root) and simple_bst(root.right, root, right)


l_ = []


def inorder(node):
    if node:
        inorder(node.left)
        l_.append(node.data)
        inorder(node.right)


node4 = Node(4)
node2 = Node(2)
node5 = Node(5)
node1 = Node(1)
node3 = Node(3)
# node7 = Node(7)

node4.left = node2
node4.right = node5
node2.left = node1
node2.right = node3
# node2.right = node7

# print is_bst_util(node4)
# print(simple_bst(node4))
inorder(node4)
print(sorted(l_) == l_)
