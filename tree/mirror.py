class Node(object):

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def mirror(self, current_node):
        if not current_node:
            return None
        mirrored_left = self.mirror(current_node.left)
        mirrored_right = self.mirror(current_node.right)
        current_node.right = mirrored_left
        current_node.left = mirrored_right
        return current_node

    def inorder(self, current_node):
        if current_node:
            self.inorder(current_node.left)
            print(current_node.data)
            self.inorder(current_node.right)


node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)
node5 = Node(5)

node1.left = node2
node1.right = node3
node2.left = node4
node2.right = node5

node1.inorder(node1)
print("\n")
node1.mirror(node1)
node1.inorder(node1)
