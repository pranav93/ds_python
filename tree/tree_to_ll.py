class Node(object):

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def convert_to_dll(node):
    if not node:
        return
    if node.left:
        left_node = convert_to_dll(node.left)
        while left_node.right:
            left_node = left_node.right
        node.left = left_node
        left_node.right = node
    if node.right:
        right_node = convert_to_dll(node.right)
        while right_node.left:
            right_node = right_node.left
        node.right = right_node
        right_node.left = node
    return node


def traverse_inorder(node):
    if node and node.data:
        traverse_inorder(node.left)
        print(node.data)
        traverse_inorder(node.right)


def find_left_most(node):
    while node.left:
        node = node.left
    return node


def traverse_ll(node):
    while node:
        print node.data
        node = node.right


node10 = Node(10)
node12 = Node(12)
node15 = Node(15)
node25 = Node(25)
node30 = Node(30)
node36 = Node(36)

node10.left = node12
node10.right = node15
node12.right = node30
node12.left = node25
node15.left = node36

traverse_inorder(node10)
print("\n")

node10 = convert_to_dll(node10)
left_most = find_left_most(node10)
traverse_ll(left_most)
