class Queue(object):
    def __init__(self):
        self.tail = -1
        self.list = []

    def enq(self, data):
        self.tail += 1
        self.list.append(data)

    def deq(self):
        if not self.list:
            raise ValueError('Queue is empty')
        else:
            self.tail -= 1
            return self.list.pop(0)

    def is_empty(self):
        return self.tail == -1


queue = Queue()


class Node(object):
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def level_order_traversal(self):
        queue.enq(self)
        while not queue.is_empty():
            an_obj = queue.deq()
            if an_obj:
                print an_obj.data
                queue.enq(an_obj.left)
                queue.enq(an_obj.right)


root = Node(2)
root.left = Node(1)
root.right = Node(3)
# root.left.left = Node(4)
root.left.right = Node(6)
root.right.left = Node(2)
# root.right.right = Node(5)

print "Level order traversal of binary tree is -"
root.level_order_traversal()
