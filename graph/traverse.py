from collections import defaultdict


class Node(object):

    def __init__(self, data):
        self.data = data
        self.visited = False


class Queue(object):

    def __init__(self):
        self.array = []
        self.tail = -1

    def enq(self, node):
        self.tail += 1
        self.array.append(node)

    def deq(self):
        self.tail -= 1
        return self.array.pop(0)

    def is_empty(self):
        return self.tail == -1


class Graph(object):

    def __init__(self):
        self.graph = defaultdict(list)

    def add_edge(self, node1, node2):
        self.graph[node1].append(node2)

    def level_order_traversal(self, node):
        q = Queue()
        q.enq(node)
        node.visited = True
        while not q.is_empty():
            current_node = q.deq()
            print(current_node.data)
            for a_node in self.graph[current_node]:
                if not a_node.visited:
                    q.enq(a_node)
                    a_node.visited = True

    def depth_order_traversal(self, node):
        node.visited = True
        print(node.data)
        for a_node in self.graph[node]:
            if not a_node.visited:
                self.depth_order_traversal(a_node)

    def depth_order_traversal_disconnected(self, node):
        for a_node in self.graph[node]:
            if not a_node.visited:
                self.depth_order_traversal(a_node)


g = Graph()
node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
g.add_edge(node0, node1)
g.add_edge(node0, node2)
g.add_edge(node1, node2)
g.add_edge(node2, node0)
g.add_edge(node2, node3)
g.add_edge(node3, node3)

# print "Following is Breadth First Traversal (starting from vertex 2)"
# g.level_order_traversal(node2)
print "Following is Depth First Traversal (starting from vertex 2)"
g.depth_order_traversal(node2)
# g.depth_order_traversal_disconnected()
