import sys

from collections import defaultdict


class Node(object):

    def __init__(self, data):
        self.data = data
        self.distance = sys.maxint
        self.weight = defaultdict(int)


def get_min(all_elem):
    return reduce(lambda x, y: x if x.distance < y.distance else y, all_elem)


class Graph(object):

    def __init__(self):
        self.graph = defaultdict(list)
        self.all_nodes = set()

    def add_edge(self, node1, node2, weight):
        self.graph[node1].append(node2)
        node2.weight[node1] = weight
        node1.weight[node2] = weight
        self.all_nodes.add(node1)
        self.all_nodes.add(node2)

    def find_shortest_path(self, source):
        non_inc = self.all_nodes.copy()
        inc = list()
        source.distance = 0
        while non_inc:
            minimum_node = get_min(non_inc)
            # non_inc.remove(minimum_node)
            inc.append(minimum_node)
            for a_node in self.graph[minimum_node]:
                if a_node.distance > minimum_node.distance + a_node.weight[minimum_node]:
                    a_node.distance = minimum_node.distance + a_node.weight[minimum_node]
        print non_inc
        print [a_node.data for a_node in inc]
        print [a_node.distance for a_node in inc]


node0 = Node(0)
node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)
node5 = Node(5)
node6 = Node(6)
node7 = Node(7)
node8 = Node(8)


graph = Graph()

graph.add_edge(node0, node1, 4)
graph.add_edge(node0, node7, 8)

graph.add_edge(node1, node2, 8)
graph.add_edge(node1, node7, 11)

graph.add_edge(node2, node3, 7)
graph.add_edge(node2, node5, 4)
graph.add_edge(node2, node8, 2)

graph.add_edge(node3, node4, 9)
graph.add_edge(node3, node5, 14)

graph.add_edge(node4, node5, 10)

graph.add_edge(node5, node6, 2)

graph.add_edge(node6, node7, 1)
graph.add_edge(node6, node8, 6)

graph.add_edge(node7, node8, 7)

graph.find_shortest_path(node0)
