class LRUQueue(object):

    def __init__(self):
        self.list = []
        self.size = 3
        self.cache_hit = 0
        self.page_fault = 0

    def enq(self, data):
        self.list.append(data)

    def deq(self):
        return self.list.pop(0)

    def refer_page(self, page):
        try:
            data_index = self.list.index(page)
        except ValueError:
            data_index = None
        if data_index is not None:
            self.cache_hit += 1
            page_item = self.list.pop(data_index)
            self.enq(page_item)
        else:
            self.page_fault += 1
            if self.size <= len(self.list):
                self.deq()
            self.enq(page)


ref_list = [1, 2, 3, 4, 1, 2, 5, 1, 2, 3, 4, 5]
lru = LRUQueue()
[lru.refer_page(a_page) for a_page in ref_list]
print lru.list
print lru.cache_hit
print lru.page_fault
