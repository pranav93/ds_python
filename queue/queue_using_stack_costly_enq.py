class Stack(object):

    def __init__(self):
        self.list = []
        self.top = -1

    def push(self, data):
        self.top += 1
        self.list.append(data)

    def pop(self):
        if not self.is_empty():
            self.top -= 1
            return self.list.pop()
        raise ValueError('Stack is empty')

    def is_empty(self):
        return self.top == -1


class Queue(object):

    def __init__(self):
        self.stack1 = Stack()
        self.stack2 = Stack()

    def enq(self, data):
        while not self.stack1.is_empty():
            pop_data = self.stack1.pop()
            self.stack2.push(pop_data)
        self.stack1.push(data)
        while not self.stack2.is_empty():
            pop_data = self.stack2.pop()
            self.stack1.push(pop_data)

    def deq(self):
        return self.stack1.pop()


q = Queue()
q.enq(1)
q.enq(2)
q.enq(3)
q.enq(4)
print(q.deq())
print(q.deq())
q.enq(5)
q.enq(6)
print(q.deq())
print(q.deq())
print(q.deq())
print(q.deq())
