def quick(arr):
    pivot_list = []
    less = []
    more = []
    if len(arr) < 1:
        return arr
    pivot = arr[0]
    for i in arr:
        if i < pivot:
            less.append(i)
        elif i > pivot:
            more.append(i)
        elif i == pivot:
            pivot_list.append(i)
    less = quick(less)
    more = quick(more)
    return less + pivot_list + more


some_arr = [5, 2, 3, 1, 4]
print quick(some_arr)
