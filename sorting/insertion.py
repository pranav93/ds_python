def insertion_sort(array):
    for slot in xrange(1, len(array)):
        value = array[slot]
        test_slot = slot - 1
        while (test_slot > -1) and array[test_slot] > value:
            array[test_slot + 1] = array[test_slot]
            test_slot -= 1
        array[test_slot + 1] = value
    return array


some_arr = [5, 2, 3, 1, 4]
print insertion_sort(some_arr)
