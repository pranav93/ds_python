def merge_sort(arr):
    arr_len = len(arr)
    if arr_len == 1:
        return arr
    arr1 = arr[arr_len/2:]
    arr2 = arr[:arr_len/2]
    sorted_arr1 = merge_sort(arr1)
    sorted_arr2 = merge_sort(arr2)
    sorted_arr1_len = len(sorted_arr1)
    sorted_arr2_len = len(sorted_arr2)
    combined_arr = []
    i = 0
    j = 0
    while (i < sorted_arr1_len) and (j < sorted_arr2_len):
        if sorted_arr1[i] <= sorted_arr2[j]:
            combined_arr.append(sorted_arr1[i])
            i += 1
        else:
            combined_arr.append(sorted_arr2[j])
            j += 1

    while i < sorted_arr1_len:
        combined_arr.append(sorted_arr1[i])
        i += 1
    while j < sorted_arr2_len:
        combined_arr.append(sorted_arr2[j])
        j += 1

    return combined_arr


some_arr = [5, 2, 3, 1, 4]
xy = merge_sort(some_arr)
print xy
