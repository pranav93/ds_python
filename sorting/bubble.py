def bubble(arr):
    for i in range(len(arr)):
        for j in range(i, len(arr)):
            if arr[i] > arr[j]:
                k = arr[i]
                arr[i] = arr[j]
                arr[j] = k
    return arr


some_arr = [5, 2, 3, 1, 4]
print bubble(some_arr)
